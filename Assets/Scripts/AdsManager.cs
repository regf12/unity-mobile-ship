using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using GoogleMobileAds;
using GoogleMobileAds.Api;

public class AdsManager : MonoBehaviour
{
		// private string _adUnitId = "ca-app-pub-3940256099942544/3419835294"; // app open
		// private string _adUnitId = "ca-app-pub-3940256099942544/6300978111"; // banner
		// private string _adUnitId = "ca-app-pub-3940256099942544/1033173712"; // interstitial
		private string _adUnitId = "ca-app-pub-3940256099942544/5224354917"; // rewarded

		private Timer TimerScript;
		private Button boton;
		private RewardedAd rewardedAd;

    void Start()
    {
				TimerScript = FindObjectOfType<Timer>();

				boton = GetComponent<Button>();
				boton.onClick.AddListener(ShowRewardedAd);

				MobileAds.RaiseAdEventsOnUnityMainThread = true;
				MobileAds.Initialize((InitializationStatus initStatus) =>
        {
            
        });

				LoadRewardedAd();
    }
		
		public void LoadRewardedAd()
    {
				if (rewardedAd != null)
				{
							rewardedAd.Destroy();
							rewardedAd = null;
				}

				Debug.Log("... Loading the rewarded ad.");

				var adRequest = new AdRequest();
				adRequest.Keywords.Add("unity-admob-sample");

				RewardedAd.Load(_adUnitId, adRequest,
						(RewardedAd ad, LoadAdError error) =>
						{
								if (error != null || ad == null)
								{
										Debug.LogError("Rewarded ad failed to load an ad " + "with error : " + error);
										return;
								}

								Debug.Log("... Rewarded ad loaded with response : " + ad.GetResponseInfo());

								rewardedAd = ad;

								RegisterEventHandlers(rewardedAd);
						});
    }

		public void ShowRewardedAd()
		{
				const string rewardMsg = "Rewarded ad rewarded the user. Type: {0}, amount: {1}.";

				if (rewardedAd != null && rewardedAd.CanShowAd())
				{
						rewardedAd.Show((Reward reward) =>
						{
								// TODO: Reward the user.
								Debug.Log(string.Format(rewardMsg, reward.Type, reward.Amount));
								
								TimerScript.AddTime();
								
								LoadRewardedAd();
						});
				}
		}

		public void RegisterEventHandlers(RewardedAd ad)
		{
				if (rewardedAd != null)
				{
						// Raised when the ad is estimated to have earned money.
						ad.OnAdPaid += (AdValue adValue) =>
						{
								Debug.Log(string.Format("#... Rewarded ad paid {0} {1}.",
										adValue.Value,
										adValue.CurrencyCode));
						};
						// Raised when an impression is recorded for an ad.
						ad.OnAdImpressionRecorded += () =>
						{
								Debug.Log("#... Rewarded ad recorded an impression.");
						};
						// Raised when a click is recorded for an ad.
						ad.OnAdClicked += () =>
						{
								Debug.Log("#... Rewarded ad was clicked.");
						};
						// Raised when an ad opened full screen content.
						ad.OnAdFullScreenContentOpened += () =>
						{
								Debug.Log("#... Rewarded ad full screen content opened.");
						};
						// Raised when the ad closed full screen content.
						ad.OnAdFullScreenContentClosed += () =>
						{
								Debug.Log("#... Rewarded ad full screen content closed.");
						};
						// Raised when the ad failed to open full screen content.
						ad.OnAdFullScreenContentFailed += (AdError error) =>
						{
								Debug.LogError("#... Rewarded ad failed to open full screen content " + "with error : " + error);
						};
				}
		}
		
}
