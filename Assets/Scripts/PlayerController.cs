using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerController : MonoBehaviour
{
		public float moveSpeed;

		// [SerializeField]
		private Timer TimerScript;

    // Start is called before the first frame update
    void Start()
    {
				TimerScript = FindObjectOfType<Timer>();
    }

    // Update is called once per frame
    void Update()
    {

				if (Input.GetKey("left"))
				{
        	gameObject.GetComponent<Transform>().position = new Vector3(gameObject.GetComponent<Transform>().position.x - moveSpeed * Time.deltaTime, -4, 0);
				}
				
				if (Input.GetKey("right"))
				{
        	gameObject.GetComponent<Transform>().position = new Vector3(gameObject.GetComponent<Transform>().position.x + moveSpeed * Time.deltaTime, -4, 0);
				}
    }

		void OnTriggerEnter2D(Collider2D other)
		{
				if (other.CompareTag("Obstacle"))
				{
						transform.position = new Vector3(0, -4, 0);

						TimerScript.Reset();

						var obstaculos = FindObjectsOfType<Obstacle>();
						foreach (var obstaculo in obstaculos)
						{
								Destroy(obstaculo.gameObject);
						}
				}
		}

		public void PushLeft()
		{
				gameObject.GetComponent<Transform>().position = new Vector3(gameObject.GetComponent<Transform>().position.x - moveSpeed * Time.deltaTime, -4, 0);
		}

		public void PushRight()
		{
				gameObject.GetComponent<Transform>().position = new Vector3(gameObject.GetComponent<Transform>().position.x + moveSpeed * Time.deltaTime, -4, 0);
		}
}
