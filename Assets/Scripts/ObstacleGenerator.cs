using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ObstacleGenerator : MonoBehaviour
{

		public GameObject obstacle;
		public float timeToInstantiate;
		public Vector3 offsetPosition;

    // Start is called before the first frame update
    void Start()
    {
        InvokeRepeating("CreateObstacle", timeToInstantiate, timeToInstantiate);
    }

    // Update is called once per frame
    void Update()
    {
        
    }

		void CreateObstacle()
		{
				var positions = new Vector3(Random.Range(2.22f, 6.33f), 9, 0);

				Instantiate(obstacle, positions, Quaternion.identity);
		}
}
