using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Timer : MonoBehaviour
{
		public static float timeStart = 0;
		public Text textBox;

    // Start is called before the first frame update
    void Start()
    {
        textBox.text = "Score: " + timeStart.ToString("f0");
    }

    // Update is called once per frame
    void Update()
    {
        timeStart += Time.deltaTime;
				textBox.text = "Score: " + timeStart.ToString("f0");
    }

    // Update is called once per frame
    public void Reset()
    {
        timeStart = 0;
    }
    
		// Update is called once per frame
    public void AddTime()
    {
        timeStart += Time.deltaTime + 10;
    }
}
